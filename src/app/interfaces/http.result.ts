export interface HttpResult {
    status?: boolean;
    result: any;
}
