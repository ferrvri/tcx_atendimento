export interface User {
    usu_id: number;
    usu_nome: string;
    usu_email: string;
    usu_login: string;
}
