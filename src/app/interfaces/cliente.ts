import { Sistema } from "./Sistema";

export interface Cliente {
    cli_id: number;
    cli_razaoSocial: string;
    cli_nomeFantasia: string;
    cli_email: string;
    cli_login: string;
    cli_status: string;
    cli_horas_bonus: number;
    cli_confirmado: number;
    sistema: Array<Sistema>;
}
