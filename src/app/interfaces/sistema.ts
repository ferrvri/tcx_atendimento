export interface Sistema {
    sis_id: number;
    sis_nome: string;
    sis_status: string;
    
    fk_tipo?: number;
    aux_cliente_sistema_id?: number;
}
