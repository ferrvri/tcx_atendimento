import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/default-side/main/main.component';
import { MainClienteComponent } from './pages/cliente-side/main-cliente/main-cliente.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/default-side/main/childrens/dashboard/dashboard.component';
import { DashboardClienteComponent } from './pages/cliente-side/main-cliente/childrens/dashboard-cliente/dashboard-cliente.component';
import { SolicitacaoComponent } from './pages/default-side/main/childrens/solicitacao/solicitacao.component';

const ROUTES: Routes = [
  {path: 'login', component: LoginComponent},

  {path: 'cliente', component: MainClienteComponent, children: [
    {path: 'dashboardCliente', component: DashboardClienteComponent},
    {path: 'solicitacao', component: SolicitacaoComponent}
  ]},

  {path: 'default', component: MainComponent, children: [
    {path: 'dashboard', component: DashboardComponent},
    {path: 'solicitacao', component: SolicitacaoComponent}  
  ]},

  

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    MainClienteComponent,
    DashboardComponent,
    DashboardClienteComponent,
    SolicitacaoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
