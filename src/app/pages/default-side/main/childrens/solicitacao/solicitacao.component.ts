import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpResult } from 'src/app/interfaces/http.result';

@Component({
  selector: 'app-solicitacao',
  templateUrl: './solicitacao.component.html',
  styleUrls: ['./solicitacao.component.css']
})
export class SolicitacaoComponent implements OnInit {

  pageParams;
  fotos = []

  constructor(private _http: HttpClient, private _router: Router, private _pageParams: ActivatedRoute) { }

  ngOnInit() {
    this._pageParams.paramMap.subscribe( e => this.pageParams = e );
    this._http.post(
      'http://technoxinformatica.com.br:21099/tcxatend/getImgsSolicitacao',
      {
        sol_id: this.pageParams.params.id || this.pageParams.params.sol_id
      }
    ).subscribe( (response: HttpResult) => {
      if (response.result && response.result.length > 0){
        this.fotos = response.result
      }
    })
  }

  showImage(arg){
    let div = document.createElement('div');
    div.innerHTML = `
      <img src="${arg}" style="width: 100%; height: 100%;"/>
    `
    swal({
      content: (div as any),
      className: 'modal-95'
    })
  }

  gerarDemanda(c) {
    let div = document.createElement('div');
    let inner =
      ` <div>
    
            <div class="form-group">
              <label>Descrição</label>
              <textarea id="detalhes" maxlength="120" class="form-control" placeholder="Detalhes"></textarea>
            </div>

            <div class="form-group">
              <label>Prioridade (0 - 5)</label>
              <input id="prior" class="form-control" placeholder="Prioridade (0 - 5)" value="1" />
            </div>
    
          </div>`;

    div.innerHTML = inner;

    swal({
      title: 'Abrir nova demanda',
      content: (div as any),
      className: 'modal-95'
    }).then((data) => {
      this._http.post(
        'http://technoxinformatica.com.br:21099/tcxatend/setDemandaCliente',
        {
          cham_id: c.id || c.sol_id,
          dem_resultado: 0,
          dem_detalhes: (document.getElementById('detalhes') as HTMLInputElement).value,
          dem_prioridade: (document.getElementById('prior') as HTMLInputElement).value,
          fk_aux_cliente_sistema_id: c.sis_id
        }
      ).subscribe((response) => {
        swal({
          icon: 'success',
          title: 'Demanda gerada com sucesso!'
        }).then( () => {
          this._router.navigate(['/default/dashboard'])
         })
      })
    });
  }

  finalizar(c){
    swal({
      title: 'Deseja realmente finalizar?',
      buttons: {
        sim:{
          text: 'Sim',
          value: true
        },
        nao: {
          text: 'Não, cancelar',
          value: false
        }
      }
    }).then( (data) => {
     if (data == true){
       this._http.post(
         'http://technoxinformatica.com.br:21099/tcxatend/finalizarSolicitacao',
         {
           sol_id: c.id || c.sol_id
         }
       ).subscribe( (response: HttpResult) => {
         if (response.status){
           swal({
             icon: 'success',
             title: 'Finalizada!'
           }).then( () => {
            this._router.navigate(['/default/dashboard'])
           })
         }
       });
     }
    });
  }

}
