import { Component, OnInit } from '@angular/core';
import { SocketIOService } from 'src/app/socket-io.service';
import io from 'socket.io-client';
import { HttpClient } from '@angular/common/http';
import { HttpResult } from 'src/app/interfaces/http.result';
import swal from 'sweetalert';
import { del } from 'selenium-webdriver/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  chamados = []
  demandas = []

  _socket = io("http://technoxinformatica.com.br:21099")

  constructor(private _http: HttpClient, private _router: Router) { }

  ngOnInit(k?) {
    this._socket.emit('join', { type: 'user', data: JSON.parse(localStorage.getItem('session')) })

    this._http.post(
      'http://technoxinformatica.com.br:21099/tcxatend/getSolicitacaoAll',
      {}
    ).subscribe((response: HttpResult) => {
      if (response.result.length > 0) {
        response.result.forEach((element) => {
          this.chamados.push(element)
        });
      }
    })

    this._http.post(
      'http://technoxinformatica.com.br:21099/tcxatend/getDemandaAll',
      {}
    ).subscribe((response: HttpResult) => {
      if (response.result.length > 0) {
        this.demandas = response.result;
        console.log(this.demandas)
      }
    })

    if (k === undefined) {
      this._socket.on('new_chamado', (data) => {
        this.chamados.push(data);
        this.chamados = this.chamados.sort((a, b) => {
          return b.id - a.id
        });
      });
    }

  }

  showDetailDemanda(d){
    (swal as any)({
      title: 'Detalhes da Demanda #' + d.dem_id,
      text: d.dem_detalhes,
      className: 'modal-70'
    });
  }

  showDetail(c){
    this._router.navigate(['/default/solicitacao', c]);
  }
  
  showDetail_bak(c) {
    let btn = {
      cancel: "Cancelar",
      demanda: {
        text: "Gerar demanda",
        value: "demanda",
      },
      finalizar:{
        text: 'Finalizar!',
        value: 'finalizar'
      }
    };
    if (c.classe == 'Analisando') {
      delete btn.demanda;
    }else{
      delete btn.finalizar
    }

    (swal as any)({
      title: 'Detalhes do chamado #' + c.id,
      text: c.detalhes,
      className: 'modal-70',
      buttons: btn
    }).then((data) => {
      if (data == 'demanda') {
        let div = document.createElement('div');
        let inner =
          ` <div>
    
            <div class="form-group">
              <label>Descrição</label>
              <textarea id="detalhes" maxlength="120" class="form-control" placeholder="Detalhes"></textarea>
            </div>

            <div class="form-group">
              <label>Descrição</label>
              <input id="prior" class="form-control" placeholder="Prioridade (0 - 5)" value="1" />
            </div>
    
          </div>`;

        div.innerHTML = inner;

        console.log(c);
        swal({
          title: 'Abrir nova demanda',
          content: (div as any),
          className: 'modal-95'
        }).then((data) => {
          this._http.post(
            'http://technoxinformatica.com.br:21099/tcxatend/setDemandaCliente',
            {
              cham_id: c.id,
              dem_resultado: 0,
              dem_detalhes: (document.getElementById('detalhes') as HTMLInputElement).value,
              dem_prioridade: (document.getElementById('prior') as HTMLInputElement).value,
              fk_aux_cliente_sistema_id: c.sis_id,
            }
          ).subscribe((response) => {
            swal({
              icon: 'succes',
              title: 'Demanda gerada com sucesso!'
            })
            this.demandas = []
            this.chamados = []
            this.ngOnInit('update')
          })
        });
      }else if (data == 'finalizar'){
         swal({
           title: 'Deseja realmente finalizar?',
           buttons: {
             sim:{
               text: 'Sim',
               value: true
             },
             nao: {
               text: 'Não, cancelar',
               value: false
             }
           }
         }).then( (data) => {
          if (data == true){
            this._http.post(
              'http://technoxinformatica.com.br:21099/tcxatend/finalizarSolicitacao',
              {
                sol_id: c.id
              }
            ).subscribe( (response: HttpResult) => {
              if (response.status){
                swal({
                  title: 'Finalizada!'
                })

                this.chamados = []
                this.demandas = [];
                this.ngOnInit('update')
              }
            });
          }
         });
      }
    });
  }

  finalizarDemanda(d){
    swal({
      title: 'Deseja realmente finalizar a demanda?',
      buttons: {
        sim: {
          text: 'Sim',
          value: true
        },
        nao: {
          text: 'Não, cancelar',
          value: false
        }
      }
    }).then( (data) => {
      if (data == true){
        this._http.post(
          'http://technoxinformatica.com.br:21099/tcxatend/finalizarDemanda',
          {
            
          }
        ).subscribe( (response: HttpResult) => {
          if (response.status){

          }
        });
      }
    })
  }

}
