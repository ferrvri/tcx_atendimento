import { Component, OnInit } from '@angular/core';
import { UserDAOService } from 'src/app/services/userDAO/user-dao.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private _userDAO: UserDAOService, private _router: Router ) { }

  ngOnInit() {
  }

  logOut(){
    console.log(this._userDAO.getActiveUser());
    if (this._userDAO.remUser()){
      this._router.navigate(['/login']);
    }
  }
}
