import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDAOService } from 'src/app/services/userDAO/user-dao.service';
import { ClienteDAOService } from 'src/app/services/clienteDAO/cliente-dao.service';

@Component({
  selector: 'app-main-cliente',
  templateUrl: './main-cliente.component.html',
  styleUrls: ['./main-cliente.component.css']
})
export class MainClienteComponent implements OnInit {

  yearNow: number = 2019;

  constructor(private _router: Router, private _clienteDAO: ClienteDAOService) { }

  ngOnInit() {
    this.yearNow = new Date().getFullYear();
  }

  logOut(){
    localStorage.removeItem('session');
    this._router.navigate(['/login']);
  }
}
