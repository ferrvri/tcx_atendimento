import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { ClienteDAOService } from 'src/app/services/clienteDAO/cliente-dao.service';
import { Utils } from 'src/app/libs/utils';

@Component({
  selector: 'app-dashboard-cliente',
  templateUrl: './dashboard-cliente.component.html',
  styleUrls: ['./dashboard-cliente.component.css']
})
export class DashboardClienteComponent implements OnInit {

  chamados_total = 0;
  chamados_fechados = 0;
  chamados_abertos = 0;
  horas_bonus = 0;

  chamados = [];
  anexos = [];

  constructor(public _clienteDAO: ClienteDAOService, private _http: HttpClient, private _router: Router) { }

  ngOnInit() {
    if (localStorage.getItem('session')) {
      this._clienteDAO = JSON.parse(localStorage.getItem('session'));
    }

    this.getDemandas();
    this.horas_bonus = this._clienteDAO.cli_horas_bonus;
    this.getSolicitacoes();
  }

  public getImgB4(event) {
    let reader = new FileReader();
    if (event.files && event.files.length > 0) {
        let file = event.files[0];
        reader.readAsDataURL(file);

        reader.onload = () => {
          console.log(reader.result);
          this.anexos.push(reader.result);
        };
    }
}

  getDemandas() {
    let sisys = []

    this._clienteDAO.sistema.forEach(element => {
      sisys.push(element.aux_cliente_sistema_id)
    });

    this._http.post(
      'http://technoxinformatica.com.br:21099/tcxatend/getDemandaCliente',
      {
        fk_aux_sistema: sisys
      }
    ).subscribe((response: any) => {
      if (response.result) {
        if (response.result.length > 0) {
          let total_h = 0;
          response.result.forEach(element => {
            total_h += Math.abs(
              new Date(element.dem_hora_fim).getTime()
              -
              new Date(element.dem_hora_inicio).getTime()
            ) / 36e5
          });

          this._clienteDAO.cli_horas_bonus -= total_h;

          this._clienteDAO.setCliente(this._clienteDAO.getActiveCliente());
        }
      }
    })
  }

  getSolicitacoes() {
    this.chamados = [];

    this.chamados_abertos = 0;
    this.chamados_fechados = 0;
    this.chamados_total = 0;

    let sisys = []

    this._clienteDAO.sistema.forEach(element => {
      sisys.push(element.aux_cliente_sistema_id)
    });

    this._http.post(
      'http://technoxinformatica.com.br:21099/tcxatend/getSolicitacaoCliente',
      {
        fk_aux_sistema: sisys
      }
    ).subscribe((response: any) => {
      if (response.result) {
        if (response.result.length > 0) {
          this.chamados = response.result;
          response.result.forEach(e => {
            if (e.sol_classe == 'Aberto') {
              this.chamados_abertos += 1;
            } else if (e.sol_classe == 'Fechado') {
              this.chamados_fechados += 1;

            }
            this.chamados_total++;
          });
        }
      }
    })
  }

  showDetail(c){
    this._router.navigate(['/cliente/solicitacao', c]);
  }

  showDetail_bak(c) {
    swal({
      title: 'Detalhes do chamado #' + c.sol_id,
      text: c.sol_detalhes
    });
  }

  novoChamado() {
    let div = document.createElement('div');
    let inner =
      ` <div>
        <div class="form-group">
          <label>Sistema</label>
          <select id="sistema_select" class="form-control">
            %SELECT%
          </select>
        </div>

        <div class="form-group">
          <label>Tipo</label>
          <select id="tipo_select" class="form-control">
            <option value="Desenvolvimento">Desenvolvimento</option>
            <option value="Erro">Erro</option>
            <option value="Correcao">Correção</option>
            <option value="Bug">Bug</option>
            <option value="Ajuste">Ajuste</option>
          </select>
        </div>

        <div class="form-group">
          <label>Descrição</label>
          <textarea id="cham_desc" maxlength="120" class="form-control" placeholder="Descrição do chamado"></textarea>
        </div>

          <div class="form-group">
            <label>Anexos #1</label>
            <input type="file" id="anexo-1" class="form-control"  style="height: 60px" />
          </div>
          
          <div class="form-group">
            <label>Anexos #2</label>
            <input type="file" id="anexo-2" class="form-control"  style="height: 60px" />
          </div>
          
          <div class="form-group">
            <label>Anexos #3</label>
            <input type="file" id="anexo-3" class="form-control"  style="height: 60px" />
          </div>

        </div>

      </div>`;

    let resultAppend = '';
    
    this._clienteDAO.sistema.forEach(element => {
      resultAppend += `<option value="${element.aux_cliente_sistema_id}"> ${element.sis_nome} </option>`;
    });

    inner = inner.replace(/%SELECT%/g, resultAppend)
    div.innerHTML = inner;

    swal({
      title: 'Abrir novo chamado',
      content: (div as any),
      className: 'modal-95'
    }).then((ok) => {
      if (ok) {
        let tipo = (document.getElementById('tipo_select') as HTMLSelectElement).value;
        let sistema = (document.getElementById('sistema_select') as HTMLSelectElement).value;
        let desc = (document.getElementById('cham_desc') as HTMLTextAreaElement).value;
        
        this.getImgB4((document.getElementById('anexo-1') as HTMLInputElement));
        this.getImgB4((document.getElementById('anexo-2') as HTMLInputElement));
        this.getImgB4((document.getElementById('anexo-3') as HTMLInputElement));

        setTimeout( () => {
          this._http.post(
            'http://technoxinformatica.com.br:21099/tcxatend/setSolicitacaoCliente',
            {
              detalhes: desc,
              tipo: tipo,
              fk_sis_id: sistema,
              anexos: this.anexos
            }
          ).subscribe((response: any) => {
            if (response.status == true) {
              swal({
                icon: 'info',
                title: 'Chamado aberto, aguarde para ser atendido'
              }).then(() => {
                this.getSolicitacoes()
                this.anexos = [];
              });
            } else {
              swal({
                icon: 'error',
                title: 'Erro ao inserir solicitação'
              })
            }
          })
        }, 1000)
      }
    });
  }

  abrirSolicitacao() {
    let div = document.createElement('div');
    let inner =
      ` <div>
        <div class="form-group">
          <label>Sistema</label>
          <select id="sistema_select" class="form-control">
            %SELECT%
          </select>
        </div>

        <div class="form-group">
          <label>Tipo</label>
          <select id="tipo_select" class="form-control">
            <option value="Desenvolvimento">Desenvolvimento</option>
            <option value="Erro">Erro</option>
            <option value="Correcao">Correção</option>
            <option value="Bug">Bug</option>
            <option value="Ajuste">Ajuste</option>
          </select>
        </div>

        <div class="form-group">
          <label>Descrição</label>
          <textarea id="cham_desc" maxlength="120" class="form-control" placeholder="Descrição da solicitação"></textarea>
        </div>

      </div>`;

    let resultAppend = '';
    this._clienteDAO.sistema.forEach(element => {
      resultAppend += `<option value="${element.aux_cliente_sistema_id}"> ${element.sis_nome} </option>`;
    });

    inner = inner.replace(/%SELECT%/g, resultAppend)
    div.innerHTML = inner;

    swal({
      title: 'Abrir nova solicitação',
      content: (div as any),
      className: 'modal-95'
    }).then((ok) => {
      let tipo = (document.getElementById('tipo_select') as HTMLSelectElement).value;
      let sistema = (document.getElementById('sistema_select') as HTMLSelectElement).value;
      let desc = (document.getElementById('cham_desc') as HTMLTextAreaElement).value;

      

    });
  }
}
