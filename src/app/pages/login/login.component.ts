import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import swal from 'sweetalert';
import { Router } from '@angular/router';
import { UserDAOService } from 'src/app/services/userDAO/user-dao.service';
import { ClienteDAOService } from 'src/app/services/clienteDAO/cliente-dao.service';
import { HttpResult } from 'src/app/interfaces/http.result';
import { User } from 'src/app/interfaces/user';
import { Cliente } from 'src/app/interfaces/cliente';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: string = '';
  senha: string = '';

  constructor(private __clienteDAO: ClienteDAOService, private _userDao: UserDAOService, private _http: HttpClient, private _router: Router) { }

  ngOnInit() { }

  login() {
    if (this.user.length < 1 || this.senha.length < 1) {
      swal({
        title: 'Preencha os campos',
        text: 'Os campos de login não podem ser vazios!',
        buttons: [
          'Ok'
        ],
        icon: 'warning'
      });
    } else {
      let h = this._http.post(
        'http://technoxinformatica.com.br:21099/tcxatend/login',
        {
          user: this.user,
          pass: this.senha
        }
      ).subscribe((response: HttpResult) => {
        if (response.result) {
          if (response.result.usu_id) {
            if (this._userDao.setUser(<User>response.result)) {
              this._router.navigate(['/default/dashboard']);
            }
          } else if (response.result.cli_id) {
            this._http.post(
              'http://technoxinformatica.com.br:21099/tcxatend/getSistemaCliente',
              {
                cli_id: response.result.cli_id
              }
            ).subscribe((response2: any) => {
              if (!response2.status) {
                response.result.sistema = response2.result;

                if (this.__clienteDAO.setCliente(<Cliente>response.result))
                  this._router.navigate(['/cliente/dashboardCliente']);
              }
            });
          }
        } else {
          if (response.status == false) {
            swal({
              icon: 'error',
              title: 'Usuario ou senha incorretos!'
            })
          }
        }
      });
    }
  }
}
