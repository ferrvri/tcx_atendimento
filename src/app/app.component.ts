import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  constructor(private _router: Router ) {}

  ngOnInit(): void {
    if (localStorage.getItem('session')){
      let response = JSON.parse(localStorage.getItem('session'))
      if (response.usu_id){
        this._router.navigate(['/default/dashboard']);
      }else if (response.cli_id){
        this._router.navigate(['/cliente/dashboardCliente']);
      }
    }else{
      this._router.navigate(['login']);
    }
  }
}
