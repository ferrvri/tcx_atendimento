import { Injectable } from '@angular/core';
import { Cliente } from 'src/app/interfaces/cliente';
import { Sistema } from 'src/app/interfaces/Sistema';

@Injectable({
  providedIn: 'root'
})
export class ClienteDAOService {

  private _cli_id: number;
  private _cli_razaoSocial: string;
  private _cli_nomeFantasia: string;
  private _cli_login: string;
  private _cli_email: string;
  private _cli_status: string;
  private _cli_horas_bonus: number;
  private _cli_confirmado: number;

  private _sistema: Array<Sistema>;

  constructor() { }

  /////
  public get cli_id(): number {
    return this._cli_id;
  }
  public set cli_id(value: number) {
    this._cli_id = value;
  }
  /////
  public get cli_razaoSocial(): string {
    return this._cli_razaoSocial;
  }
  public set cli_razaoSocial(value: string) {
    this._cli_razaoSocial = value;
  }
  /////
  public get cli_nomeFantasia(): string {
    return this._cli_nomeFantasia;
  }
  public set cli_nomeFantasia(value: string) {
    this._cli_nomeFantasia = value;
  }
  /////
  public get cli_login(): string {
    return this._cli_login;
  }
  public set cli_login(value: string) {
    this._cli_login = value;
  }
  /////
  public get cli_email(): string {
    return this._cli_email;
  }
  public set cli_email(value: string) {
    this._cli_email = value;
  }
  /////
  public get cli_status(): string {
    return this._cli_status;
  }
  public set cli_status(value: string) {
    this._cli_status = value;
  }
  /////
  public get cli_horas_bonus(): number {
    return this._cli_horas_bonus;
  }
  public set cli_horas_bonus(value: number) {
    this._cli_horas_bonus = value;
  }
  /////
  public get cli_confirmado(): number {
    return this._cli_confirmado;
  }
  public set cli_confirmado(value: number) {
    this._cli_confirmado = value;
  }
  /////
  public get sistema(): Array<Sistema> {
    return this._sistema;
  }
  public set sistema(value: Array<Sistema>) {
    this._sistema = value;
  }
  /////

  ////***============== */

  public setCliente(cliente: Cliente): boolean {
    console.log(cliente);
    this.cli_id = cliente.cli_id;
    this.cli_nomeFantasia = cliente.cli_nomeFantasia;
    this.cli_razaoSocial = cliente.cli_razaoSocial;
    this.cli_email = cliente.cli_email;
    this.cli_login = cliente.cli_login;
    this.cli_status = cliente.cli_status;
    this.cli_horas_bonus = cliente.cli_horas_bonus;
    this.cli_confirmado = cliente.cli_confirmado;
    this.sistema = cliente.sistema;

    localStorage.removeItem('session');
    localStorage.setItem('session', JSON.stringify(cliente));
    return true;
  }

  public remCliente(): boolean {
    localStorage.removeItem('session');
    if (!localStorage.getItem('session')) {
      return true;
    } else {
      return false;
    }
  }

  public getActiveCliente(): Cliente {
    return this;
  }

  ////***============== */
}
