import { TestBed } from '@angular/core/testing';

import { ClienteDAOService } from './cliente-dao.service';

describe('ClienteDAOService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClienteDAOService = TestBed.get(ClienteDAOService);
    expect(service).toBeTruthy();
  });
});
