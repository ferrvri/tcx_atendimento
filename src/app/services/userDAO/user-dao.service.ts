import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserDAOService {

  private _usu_id: number = 0;
  private _usu_nome: string = '';
  private _usu_email: string = '';
  private _usu_login: string = '';
  
  constructor() {  }
////  
  public get usu_id(): number {
    return this._usu_id;
  }
  public set usu_id(value: number) {
    this._usu_id = value;
  }
////
  public get usu_nome(): string {
    return this._usu_nome;
  }
  public set usu_nome(value: string) {
    this._usu_nome = value;
  }
////
  public get usu_email(): string {
    return this._usu_email;
  }
  public set usu_email(value: string) {
    this._usu_email = value;
  }
////
  public get usu_login(): string {
    return this._usu_login;
  }
  public set usu_login(value: string) {
    this._usu_login = value;
  }
////

/** ======= */

 public setUser(user: User): boolean{
    this.usu_id = user.usu_id;
    this.usu_nome = user.usu_nome;
    this.usu_email = user.usu_email;
    this.usu_login = user.usu_login;
    localStorage.setItem('session', JSON.stringify(user));
    if (localStorage.getItem('session')){
      return true;
    }else{
      return false;
    }
  }

  public remUser(): boolean{
    localStorage.removeItem('session');
    if (!localStorage.getItem('session')){
      return true;
    }else{
      return false;
    }
  }

  public getActiveUser(): User{
    return this;
  }
  
/** ======= */
}
