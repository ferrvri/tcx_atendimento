import { TestBed } from '@angular/core/testing';

import { UserDAOService } from './user-dao.service';

describe('UserDAOService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserDAOService = TestBed.get(UserDAOService);
    expect(service).toBeTruthy();
  });
});
