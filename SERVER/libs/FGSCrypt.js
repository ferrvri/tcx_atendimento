'use strict';

module.exports = {

    encryptStringWithXORtoHex: (input, key) => {
        var c = '';

        while (key.length < input.length) {
             key += key;
        }
        
        for(var i=0; i<input.length; i++) {
            let value1 = input[i].charCodeAt(0);
            let value2 = key[i].charCodeAt(0);

            let xorValue = value1 ^ value2;

            let xorValueAsHexString = xorValue.toString("16");
    
            if (xorValueAsHexString.length < 2) {
                xorValueAsHexString = "0" + xorValueAsHexString;
            }
    
            c += xorValueAsHexString;
        }
        return c;
    },

    decryptStringWithXORFromHex: (input, key) => {
        let c = '';
        while (key.length < input.length/2) {
            key += key;
        }
    
        for (let i=0; i < input.length; i += 2) {
            let hexValueString = input.substring(i, i+2);
            let value1 = parseInt(hexValueString, 16);
            let value2 = key.charAt(i/2);
            let xorValue = value1 ^ value2;

            c += xorValue.toString();
        }
        return c;
    }
};