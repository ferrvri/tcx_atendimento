'use strict';

const app = require('express')();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const cors = require('cors');
const io = require('socket.io')(http);

const dbConnection = require('./libs/dbConnection');
const fgs = require('./libs/FGSCrypt');
const fs = require('fs');

const mailer = require('./libs/mail');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }))
app.use(bodyParser.json({
    limit: '50mb'
}));

const connection = dbConnection.mysql.connection;
const HTTP_PORT = 21099;

let CLIENTES = [];
let USUARIOS = [];
let CHATS = [];

if (connection) {
    console.log('MYSQL connected');
    // console.log(fgs.encryptStringWithXORtoHex('test', 'mykey'));
    // console.log('dec', fgs.decryptStringWithXORFromHex('x41tCHD', 'teste'));
} else {
    console.error('MYSQL not connected');
}

/************** Login *************/
app.post('/tcxatend/login', (req, res) => {
    if (req.body.user && req.body.pass) {
        connection.query('select usu_id, usu_login, usu_nome, usu_email from usuario where usu_login = ? and usu_senha = ? and bit_deletado = 0', [req.body.user, req.body.pass], (err, result, fields) => {
            if (err || result.length < 1) {
                console.log(err);
                connection.query('select cli_id, cli_razaoSocial, cli_email, cli_login, cli_status, cli_horas_bonus, cli_confirmado from cliente where (cli_email = ? or cli_login = ?) and cli_senha = ? and bit_deletado = 0', [req.body.user, req.body.user, req.body.pass], (err2, result2, fields) => {
                    if (err2 || result2.length < 1) {
                        res.end(JSON.stringify({ "status": false }));
                    } else {
                        res.end(JSON.stringify({ result: result2[0] }));
                    }
                });
            } else {
                res.end(JSON.stringify({ result: result[0] }));
            }
        });
    }
});

io.on('connection', (socket) => {
    socket.on('join', (data) => {
        if (data.type == 'user') {
            USUARIOS.push({
                _data: data.data,
                socket: socket
            })
            console.log('[INFO] New cliente connected - ' + data.data.usu_nome);
        }
    });
});

/***************************/

/************** Cliente e Sistema *************/
app.post('/tcxatend/getSistemaCliente', (req, res) => {
    if (req.body.cli_id) {
        connection.query('select aux_cliente_sistema_id, sis_id, sis_nome from aux_cliente_sistema inner join sistema on fk_aux_sistema_id = sis_id where sistema.bit_deletado = 0 and aux_cliente_sistema.bit_deletado = 0 and fk_aux_cliente_id = ?', [req.body.cli_id], (err, result, fields) => {
            if (err || result.length < 1) {
                res.end(JSON.stringify({ "status": false }));
            } else {
                console.log(result);
                res.end(JSON.stringify({ result: result }));
            }
        });
    }
});
/***************************/


/************** Solicitações dos clientes *************/
app.post('/tcxatend/getImgsSolicitacao', (req, res) => {
    connection.query(
        'select img_url, img_id from solicitacao_images where fk_solicitacao_id = ?',
        [req.body.sol_id],
        (err, result, fields) => {
            if (err) {
                res.end(JSON.stringify({ status: false }))
            } else {
                res.end(JSON.stringify({ result: result }))
            }
        }
    )
});

app.post('/tcxatend/setSolicitacaoCliente', (req, res) => {
    if (req.body.detalhes && req.body.fk_sis_id) {
        connection.query(
            'insert into solicitacao (sol_detalhes, sol_tipo, sol_classe, fk_aux_cliente_sistema_id) values (?, ?, "Aberto", ?)',
            [req.body.detalhes, req.body.tipo, req.body.fk_sis_id],
            (err, result, fields) => {

                if (!err) {
                    if (req.body.anexos && req.body.anexos.length > 0) {
                        req.body.anexos.forEach((element, index) => {

                            fs.writeFile('../www/atendimento/assets/images_solicitacao/solicitacao-' + index + '-' + result.insertId + '-' + new Date(Date.now()).toISOString().split('T')[0] + '.png', element.split(',')[1], { encoding: 'base64' }, (e) => {
                                console.log(e)
                            });

                            connection.query(
                                'insert into solicitacao_images (img_url, fk_solicitacao_id) values (?, ?)',
                                ['solicitacao-' + index + '-' + result.insertId + '-' + new Date(Date.now()).toISOString().split('T')[0] + '.png', result.insertId],
                                (err, result, fields) => {
                                    if (err) {
                                        res.end(JSON.stringify({ status: '0x102' }))
                                        throw err;
                                    }
                                }
                            )
                        })
                    }
                    connection.query(
                        `select 
                                cli_razaoSocial, cli_email,  sis_nome, tip_servico_nome

                            from aux_cliente_sistema 

                            inner join cliente on fk_aux_cliente_id = cli_id 
                            inner join sistema on fk_aux_sistema_id = sis_id
                            inner join tipo_servico on fk_tipo = tip_servico_id
                            
                            where 
                                cliente.bit_deletado = 0 and 
                                sistema.bit_deletado = 0 and 
                                tipo_servico.bit_deletado = 0 and 
                                fk_aux_sistema_id = ?
                                    `,
                        [req.body.fk_sis_id],
                        (err, result2, fields) => {
                            if (err) {
                                console.log(err);
                            } else {
                                USUARIOS.forEach((e) => {
                                    e.socket.emit('new_chamado', {
                                        id: result.insertId,
                                        cli_email: result2[0].cli_email,
                                        cli_nome: result2[0].cli_razaoSocial,
                                        sis_nome: result2[0].sis_nome,
                                        tip_servico_nome: result2[0].tip_servico_nome,
                                        tipo: req.body.tipo,
                                        fk_sis_id: req.body.fk_sis_id,
                                        classe: 'Aberto',
                                        detalhes: req.body.detalhes,
                                        sol_data:
                                            new Date(Date.now()).toLocaleString()
                                    })
                                });

                                let date = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), new Date().getHours(), new Date().getMinutes()).toString()

                                mailer.send({
                                    from: 'victor@maisfidelizacao.com.br',
                                    to: result2[0].cli_email + ', victorfergs@hotmail.com',
                                    subject: 'Novo Chamado #' + result.insertId + ' - ' + result2[0].sis_nome,
                                    html: `
                                            <table border="0" style="border: none; width: 100%;">
                                            <thead>
                                              <tr>
                                                 <td>
                                                   <div style="text-align: center">
                                                     <p style="font-size: 16px; font-weight: bold">
                                                       TechnoX Informática
                                                     </p>
                                                         <p style="font-weight: bold; font-size: 12px">
                                                        Novo chamado #${result.insertId}
                                                     </p>
                                                   </div>
                                                </td>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                  <td>
                                                        <div style="width: 100%; display: table; margin: auto; text-align: center;">
                                                        <p>
                                                          Cliente:
                                                          <span style="font-weight: bold">${result2[0].cli_razaoSocial} - ${result2[0].cli_email}</span>
                                                        </p>
                                                        <p>
                                                          Sistema:
                                                          <span style="font-weight: bold">${result2[0].sis_nome}</span>
                                                        </p>
                                                        <p>
                                                                Tipo: 
                                                          <span style="font-weight: bold">${result2[0].tip_servico_nome}</span>
                                                        </p>

                                                        <p>
                                                          Detalhes:
                                                          <span style="font-weight: bold">${req.body.detalhes}</span>
                                                        </p>
                                                    </div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                          <footer style="text-align: center">
                                              <span>${date}</span>
                                          </footer>
                                            `
                                }).then((data) => {
                                    res.end(JSON.stringify({ "status": true }));
                                }, (er) => {
                                    res.end(JSON.stringify({ "status": false }));
                                });
                                res.end(JSON.stringify({ "status": true }));
                            }
                        }
                    )
                }
            });
    }
})

app.post('/tcxatend/getSolicitacaoAll', (req, res) => {
    connection.query(
        `select 
            sol_id as id, date_format(sol_data, '%d/%m/%Y as %H:%i') as sol_data, sol_detalhes as detalhes, sol_tipo as tipo, sol_classe as classe, sis_id, sis_nome
        from solicitacao
        inner join sistema on fk_aux_cliente_sistema_id = sis_id
        inner join aux_cliente_sistema on aux_cliente_sistema.fk_aux_sistema_id = sis_id
        inner join cliente on aux_cliente_sistema.fk_aux_cliente_id = cli_id

        where

        cliente.bit_deletado = 0 and sistema.bit_deletado = 0 and
        TIMESTAMPDIFF(DAY, sol_data, now()) <= 30 
        order by sol_id desc
        `,
        [],
        (err, result, fields) => {
            let r_result = [];
            if (err) {
                console.log('Err get solicitacao all');
            } else {
                if (result.length > 0) {
                    result.forEach((el) => {
                        r_result.push(el);
                    });
                    res.end(JSON.stringify({ result: r_result }));
                }
            }
        });
})

app.post('/tcxatend/getSolicitacaoCliente', (req, res) => {
    if (req.body.fk_aux_sistema) {
        let r_result = [];
        let s = `select 
                    sol_id, sol_classe, sol_detalhes, Date_format(sol_data, "%d/%m/%Y, %H:%m") as sol_data, sol_tipo, sis_nome 
                from solicitacao 
                inner join aux_cliente_sistema on fk_aux_cliente_sistema_id = aux_cliente_sistema_id 
                inner join sistema on aux_cliente_sistema.fk_aux_sistema_id = sis_id
                where fk_aux_cliente_sistema_id in %# and 
                sistema.bit_deletado = 0 and
                aux_cliente_sistema.bit_deletado = 0 and
                solicitacao.bit_deletado = 0 order by sol_id desc`;

        let re = '(';

        req.body.fk_aux_sistema.forEach((element, index) => {
            if (index == req.body.fk_aux_sistema.length - 1) {
                re += element + ')';
            } else {
                re += element + ',';
            }
        });
        s = s.replace(/%#/g, re);
        connection.query(
            s,
            [],
            (err, result, fields) => {

                if (err) {
                    console.log('Err get solicitacao cliente');
                } else {
                    if (result.length > 0) {
                        result.forEach((el) => {
                            r_result.push(el);
                        });
                        res.end(JSON.stringify({ result: r_result }));
                    }
                }
            });
    }
})


app.post('/tcxatend/finalizarSolicitacao', (req, res) => {
    connection.query(
        'update solicitacao set sol_classe = "Fechado" where sol_id = ?'
        , [req.body.sol_id], (err, result, fields) => {
            if (!err) {
                connection.query(
                    ` select cli_razaoSocial, cli_email, sol_detalhes, sol_data, sol_tipo, sis_nome
                        FROM solicitacao

                        inner join sistema on solicitacao.fk_aux_cliente_sistema_id = sistema.sis_id
                        inner join aux_cliente_sistema on fk_aux_cliente_sistema_id = sistema.sis_id
                        inner join cliente on aux_cliente_sistema.fk_aux_cliente_id = cli_id

                        where sol_id = ?
                    `,
                    [req.body.sol_id],
                    (err, result2, fields) => {
                        if (err) {
                            console.log(err);
                        } else {
                            let date = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), new Date().getHours(), new Date().getMinutes()).toString()

                            mailer.send({
                                from: 'victor@maisfidelizacao.com.br',
                                to: result2[0].cli_email + ', victorfergs@hotmail.com',
                                subject: 'Chamado finalizado #'+ req.body.sol_id + ' - ' + result2[0].sis_nome,
                                html: `
                            <table border="0" style="border: none; width: 100%;">
                            <thead>
                              <tr>
                                 <td>
                                   <div style="text-align: center">
                                     <p style="font-size: 16px; font-weight: bold">
                                       TechnoX Informática
                                     </p>
                                         <p style="font-weight: bold; font-size: 12px">
                                            Chamado #${req.body.sol_id}
                                     </p>
                                   </div>
                                </td>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                  <td>
                                        <div style="width: 100%; display: table; margin: auto; text-align: center;">
                                        <p>
                                            Data da solicitação: ${result2[0].sol_data}
                                        </p>

                                        <p>
                                            Sistema: ${result2[0].sis_nome}
                                        </p>
                                        <p>
                                          Tipo: ${result2[0].sol_tipo}
                                        </p>

                                        <p>
                                           Detalhes: ${result2[0].sol_detalhes}
                                        </p>

                                    </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <footer style="text-align: center">
                              <span>${date}</span>
                          </footer>
                            `
                            }).then((data) => {
                                res.end(JSON.stringify({ "status": true }));
                            }, (er) => {
                                res.end(JSON.stringify({ "status": false }));
                            });
                        }

                    });
            }
        }
    )
});

/***************************/

/******* Demanda dos clientes **********/

app.post('/tcxatend/setDemandaCliente', (req, res) => {
    if (req.body.dem_detalhes && req.body.fk_aux_cliente_sistema_id) {
        connection.query(
            'insert into demandas (dem_status, dem_resultado, dem_detalhes, dem_prioridade, fk_aux_cliente_sistema_id, fk_solicitacao_id) values ("aberta",? , ?, ?, ?, ?)',
            [req.body.dem_resultado, req.body.dem_detalhes, req.body.dem_prioridade, req.body.fk_aux_cliente_sistema_id, req.body.cham_id],
            (err, result, fields) => {
                if (err) {
                    res.end(JSON.stringify({ "status": false }));
                } else {

                    connection.query(
                        'update solicitacao set sol_classe = "Analisando" where sol_id = ?',
                        [req.body.cham_id],
                        (err, result, fields) => {
                            if (!err) {
                                res.end(JSON.stringify({ "status": true }));
                            }
                        }
                    )
                }
            });
    }
})

app.post('/tcxatend/getDemandaCliente', (req, res) => {
    if (req.body.fk_aux_sistema) {
        let r_result = [];
        let s = `select 
                    dem_id, dem_hora_inicio, dem_hora_fim, dem_resultado, dem_detalhes, dem_prioridade, sis_nome 
                from demandas 
                inner join aux_cliente_sistema on fk_aux_cliente_sistema_id = aux_cliente_sistema_id 
                inner join sistema on aux_cliente_sistema.fk_aux_sistema_id = sis_id
                where fk_aux_cliente_sistema_id in %# and 
                sistema.bit_deletado = 0 and
                aux_cliente_sistema.bit_deletado = 0 and
                demandas.bit_deletado = 0 order by dem_id desc`;

        let re = '(';

        req.body.fk_aux_sistema.forEach((element, index) => {
            if (index == req.body.fk_aux_sistema.length - 1) {
                re += element + ')';
            } else {
                re += element + ',';
            }
        });
        s = s.replace(/%#/g, re);
        connection.query(
            s,
            [],
            (err, result, fields) => {

                if (err) {
                    console.log('Err get demandas cliente');
                } else {
                    if (result.length > 0) {
                        result.forEach((el) => {
                            r_result.push(el);
                        });
                        res.end(JSON.stringify({ result: r_result }));
                    } else {
                        res.end(JSON.stringify({ status: "false" }));
                    }
                }
            });
    }
})

app.post('/tcxatend/getDemandaAll', (req, res) => {
    let s = `
            select 
                dem_id, dem_detalhes, dem_status, dem_hora_inicio, dem_hora_fim, dem_resultado, dem_prioridade, sis_nome 
            from demandas 
            
            inner join sistema on fk_aux_cliente_sistema_id = sis_id 

            where 

            demandas.bit_deletado = 0 and
            
            TIMESTAMPDIFF(DAY, dem_hora_inicio, now()) <= 30 
            order by dem_id desc
        `;
    // demandas.dem_hora_fim is null and
    connection.query(s, [], (err, result, fields) => {
        if (err) {
            return res.end(JSON.stringify({ status: 'false' }))
        } else {
            return res.end(JSON.stringify({ result: result }))
        }
    })
})

app.post('/tcxatend/getDemandaAll', (req, res) => {
    let s = `
            select 
                dem_id, dem_detalhes, dem_status, dem_hora_inicio, dem_hora_fim, dem_resultado, dem_prioridade, sis_nome 
            from demandas 
            
            inner join sistema on fk_aux_cliente_sistema_id = sis_id 

            where 

            demandas.bit_deletado = 0 and
            
            TIMESTAMPDIFF(DAY, dem_hora_inicio, now()) <= 30 
            order by dem_id desc
        `;
    // demandas.dem_hora_fim is null and
    connection.query(s, [], (err, result, fields) => {
        if (err) {
            return res.end(JSON.stringify({ status: 'false' }))
        } else {
            return res.end(JSON.stringify({ result: result }))
        }
    })
})

/***************************************/


try {
    http.listen(HTTP_PORT);
    console.log('HTTP started', HTTP_PORT);
} catch (Exception) {
    console.log('Error => ', Exception);
}
